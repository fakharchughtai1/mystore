<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterMaking(function (Category $category) {
            //
        })->afterCreating(function (Category $category) {

            if($category->id <= 10) {
                $parentId = NULL;
            }
            else if($category->id >= 11 && $category->id <=300 ) {
                $parentId = substr($category->id, -1);
                if( $parentId == 0 ) $parentId = 10;
            }
            else {
                $parentId = rand(11, 300);
            }
            $category->parent_id = $parentId;
            $category->save();
        });
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'label' => $this->faker->name,
            'parent_id' => null,
        ];
    }
}
