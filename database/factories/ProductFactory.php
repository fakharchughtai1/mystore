<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $type = ['physical', 'digital'][rand(0,1)];
        $download_url = $type == 'digital' ? '/download?id='.rand(1000, 9999) : NULL;
        $weight = $type == 'digital' ? round((rand(10, 90) / 10), 2) : NULL;
        return [
            'label' => $this->faker->name,
            'type' => $type,
            'download_url' => $download_url,
            'weight' => $weight,
            'homepage' => ['yes', 'no'][rand(0,1)]
        ];
    }
}
