<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



/**
 * Mainpage routes
 */
Route::prefix('mainpage')->group(function () {
    Route::get('products', 'App\Http\Controllers\ProductsController@homepage');
});


/**
 * Products routes
 */
Route::prefix('products')->group(function () {
    Route::get('/', 'App\Http\Controllers\ProductsController@list');
});

/**
 * Category routes
 */
Route::prefix('categories')->group(function () {
    Route::get('/', 'App\Http\Controllers\CategoryController@list');
});

/**
 * Cart routes
 */
Route::prefix('cart')->group(function () {
    Route::post('products/{product_id}', 'App\Http\Controllers\CartController@add');
    Route::delete('/', 'App\Http\Controllers\CartController@delete');
    Route::get('/', 'App\Http\Controllers\CartController@list');
});

/**
 * Orders routes
 */
Route::prefix('order')->group(function () {
    Route::post('/', 'App\Http\Controllers\OrderController@add');
    Route::get('/{order_id}', 'App\Http\Controllers\OrderController@list');
    
});