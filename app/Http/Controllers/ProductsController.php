<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Product;
use Illuminate\Support\Facades\Crypt;

class ProductsController extends Controller
{
    /**
     * List all products
     */
    public function list(Request $request, Product $product)
    {
        try{
            
            return  response(
                        $product->setPage($request)
                                ->setLimit($request, 2)
                                ->setFilters($request)
                                ->getProductsList()
                    )->header('page', Crypt::encryptString($product->getPage() + 1) );
        
        } catch(\Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }
    
    /**
     * List all products chosen to be display on homepage
     */
    public function homepage(Request $request, Product $product)
    {
        try{
            
            return  response(
                        $product->setPage($request)
                                ->setLimit($request, 8)
                                ->getHomepageProducts()
                    )->header('page', Crypt::encryptString($product->getPage() + 1) );
        
        } catch(\Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }
}
