<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Order;

class OrderController extends Controller
{

    /**
     * Place order
     */ 
    public function add(Order $order)
    {
        try{
            
            return $order->placeOrder();

        } catch(\Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }

    /**
     * Show details of an order
     */ 
    public function list(Order $order, $orderId)
    {
        return $order->getOrderDetails($orderId);
    }
}
