<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Cart;

class CartController extends Controller
{
    /**
     * Add product to the cart
     */    
    public function add(Cart $cart, $product_id)
    {
        try{
            
            return  response($cart->getCart()->addProduct($product_id)->saveCart());

        } catch(\Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }
        
    /**
     * Delete Cart
     */      
    public function delete(Cart $cart)
    {
        return $cart->deleteCart();
    }
        
    /**
     * Show Cart details
     */      
    public function list(Cart $cart)
    {
        return $cart->getCartDetails();
    }
}
