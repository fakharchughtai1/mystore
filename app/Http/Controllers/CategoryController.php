<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Category;
use Illuminate\Support\Facades\Crypt;

class CategoryController extends Controller
{
    /**
     * List all categories
     */
    public function list(Request $request, Category $category)
    {
        try{

            return  response(
                        $category->setPage($request)
                                 ->setLimit($request)
                                 ->getCategoriesList(true)
                    )->header('page', Crypt::encryptString($category->getPage() + 1) );
        
        } catch(\Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }
}
