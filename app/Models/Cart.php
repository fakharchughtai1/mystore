<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use \App\Models\Product;
use Session;
use \App\Models\CartItem;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = ['cart_token', 'updated_at'];

    private $cartId = null;
    private $cartItem = null;

    /**
     * Get cart after intializing session / cart token
     * @return Cart
     */    
    public function getCart()
    {
        $this->setCartId();
        return $this;
    }

    /**
     * Get cart session / cart token
     * @return string
     */ 
    private function setCartId()
    {
        $this->cartId = empty($this->cartId) ? Session::get('cart_token') : $this->cartId;
        if( empty($this->cartId) ) {
            $this->cartId = (string) Str::uuid();
            Session::put('cart_token', $this->cartId);
        }

        return $this->cartId;
    }

    /**
     * Get cart session / cart token
     * @return string
     */     
    private function getCartId()
    {
        if( empty($this->cartId) ) {
            $this->setCartId();
        }
        return $this->cartId;
    }

    /**
     * Add product to cart
     * @param integer
     * @return Cart
     */     
    public function addProduct($product_id)
    {
        $this->cartItem = new CartItem();
        $this->cartItem->setProduct($product_id);
        return $this;
    }

    /**
     * Get product suppose to add in cart
     * @return Product
     */    
    public function getProduct()
    {
        return $this->cartItem;
    }

    /**
     * Save cart and add products to the cart
     * @return Collection
     */        
    public function saveCart()
    {
        $cartDetails = Cart::where(['cart_token' => $this->getCartId()])->first();
        if( empty($cartDetails) ) {
            $cartDetails = Cart::create([
                'cart_token' => $this->getCartId()
            ]);
        }

        $this->getProduct()->addtoCart($cartDetails);


        return $this->getCartDetails();
    }

    /**
     * Delete cart
     */      
    public function resetCartId()
    {
        $this->cartId = NULL;
        Session::flush();
    }


    /**
     * Get cart details
     * @return Collection
     */  
    public function getCartDetails()
    {
        $cartDetails = Cart::where(['cart_token' => $this->getCartId()])->first();

        return [
            'cart_token' => $this->getCartId(),
            'items' => isset($cartDetails->id) ? CartItem::where(['cart_id' => $cartDetails->id])->get() : []
        ];
    }
    
    /**
     * Delete cart
     * @return array
     */      
    public function deleteCart()
    {
        $productsCount = Cart::where(['cart_token' => $this->getCartId()])->count();
        if( $productsCount > 0 ) {
            Cart::where(['cart_token' => $this->getCartId()])->delete();
        }
        return ['status'=> true, 'message'=>'Cart deleted successfully'];
    }
}
