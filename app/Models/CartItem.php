<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    use HasFactory;

    protected $fillable = ['cart_id', 'product_id', 'quantity', 'updated_at'];

    private $cartProduct = null;

    /**
     * Set product to be added in cart
     * @param  integer  $product_id
     * @return CartItem
     */    
    public function setProduct($product_id)
    {
        $this->cartProduct = Product::where('id', $product_id)->firstOrFail();
        return $this;
    }

    /**
     * Get product to be added in cart
     * @return Product
     */ 
    public function getProduct()
    {
        return $this->cartProduct;
    }

    /**
     * Add product in cart
     */    
    public function addToCart(\App\Models\Cart $cart)
    {
        $cartItem = CartItem::where(['cart_id' => $cart->id, 'product_id' => $this->getProduct()->id])->first();
        if( empty($cartItem) ) {
            CartItem::create([
                'cart_id' => $cart->id,
                'product_id' => $this->getProduct()->id,
                'quantity' => 1
            ]);
        }
        else {
            $cartItem->quantity= $cartItem->quantity + 1;
            $cartItem->save();
        }
    }
}
