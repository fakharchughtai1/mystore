<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;

class Category extends Model
{
    use HasFactory;

    private $page_number = null;
    private $page_limit = null;

    /**
     * Link Category to itself
     * @return HasMany
     */
    public function childCategories()
    {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id');
    }

    /**
     * Recursively include children at all levels
     * @return HasMany
     */    
    public function allChildCategories()
    {
        return $this->childCategories()->with('allChildCategories');
    }

    /**
     * Format categories response
     * @param lluminate\Database\Eloquent\Collection $categoriesList
     * @param array $formatedData
     * @return array
     */    
    private function foramtCategoriesList(\Illuminate\Database\Eloquent\Collection $categoriesList, array $formatedData = [])
    {
        foreach($categoriesList as $category) {
            $data = [
                'id' => $category->id,
                'label' => $category->label
            ];
            if( !empty($category->allChildCategories->count()) ) {
                $data['sub_categories'] = $this->foramtCategoriesList($category->allChildCategories);
            }
            $formatedData[] = $data ;
        }
        return $formatedData;
    }

    /**
     * Set page number for pagination
     * @param Request $request
     * @return Category
     */      
    public function setPage(Request $request)
    {
        if( $request->hasHeader('page') ) {
            $this->page_number = Crypt::decryptString( $request->header('page') );
        }
        else {
            $this->page_number = 1;
        }
        return $this;
    }

    /**
     * Get page number for pagination
     * @return integer
     */     
    public function getPage()
    {
        return $this->page_number;
    }


    /**
     * Set limit for pagination
     * @param Request $request
     * @return Category
     */ 
    public function setLimit(Request $request)
    {
        $this->page_limit = 2;
        return $this;
    }

    /**
     * Get limit for pagination
     * @return integer
     */     
    public function getLimit()
    {
        return $this->page_limit;
    }

    /**
     * Get list of all categories
     * @param boolean $formated
     * @return Colleciton
     */    
    public function getCategoriesList($formated = false)
    {
        $page = $this->getPage();
        $limit = $this->getLimit();
        $categoriesList = self::with('allChildCategories:id,label,parent_id')->offset( (($page-1)*$limit) )->limit($limit)->get();
        if( $formated ) {
            return $this->foramtCategoriesList($categoriesList);
        }
        else {
            return $categoriesList;
        }
    }
}
