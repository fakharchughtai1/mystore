<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    use HasFactory;

    protected $fillable = ['order_id', 'product_id', 'quantity'];

    /**
     * Each Items belongs to a product
     */      
    public function product()
    {
        return $this->hasOne(Product::class, 'id');
    }
}
