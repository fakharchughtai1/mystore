<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class Product extends Model
{
    use HasFactory;

    /**
     * Page number for pagination 
     */
    private $page_number = null;

    /**
     * Numer of products can be returned per call
     */
    private $page_limit = null;

    /**
     * array: filters for products list
     */
    private $page_filers = [];


    /**
     * Set page number for pagination
     * @param  Request  $request
     * @return Product
     */
    public function setPage(Request $request)
    {
        if( $request->hasHeader('page') ) {
            $this->page_number = Crypt::decryptString( $request->header('page') );
        }
        else {
            $this->page_number = 1;
        }
        return $this;
    }

    /**
     * Get page number for pagination
     * @return integer
     */
    public function getPage()
    {
        return $this->page_number;
    }

    /**
     * Set limit for numer of products can be returned per call
     * @param  Request $request
     * @param  integer $defaultLimi
     * @return Product
     */
    public function setLimit(Request $request, $defaultLimit)
    {
        $this->page_limit = $defaultLimit;
        if ($request->has('limit')) {

            $limit = $request->input('limit');

            if( empty($limit) || !is_numeric($limit) ) {
                throw new \Exception('Invalid value for limit');
            }

            $this->page_limit = $limit;

        }
        return $this;
    }

    /**
     * Get limit for numer of products can be returned per call
     * @return integer
     */
    public function getLimit()
    {
        return $this->page_limit;
    }

    /**
     * Set filter for list of products
     * @param  Request $request
     * @return Product
     */    
    public function setFilters(Request $request)
    {
        $this->page_filers = [];

        if ($request->has('label')) {
            $this->page_filers['label'] = $request->input('label');
        }

        if ($request->has('type')) {
            $this->page_filers['type'] = $request->input('type');
        }

        return $this;
    }

    /**
     * Get filter for list of products
     * @return array
     */ 
    public function getFilters()
    {
        return $this->page_filers;
    }

    /**
     * Get list of all products
     * @return colleciton
     */    
    public function getProductsList()
    {
        $page = $this->getPage();
        $limit = $this->getLimit();
        $oProducts = $this->select(['id','label','type','download_url','weight','homepage'])
                    ->offset( (($page-1)*$limit) )
                    ->limit($limit);
        $filters =  $this->getFilters();

        if( isset($filters['label']) ) {
            $oProducts = $oProducts->where(\DB::raw('lower(label)'), 'LIKE', \DB::raw("lower('%{$filters['label']}%')"));
        }

        if( isset($filters['type']) ) {
            $oProducts = $oProducts->where('type', $filters['type']);
        }
                    
        return $oProducts->get();
    }

    /**
     * Get list of all products chosen for display at home
     * @return colleciton
     */ 
    public function getHomepageProducts()
    {
        $page = $this->getPage();
        $limit = $this->getLimit();

        if( !in_array($limit, [8, 16]) ) {
            throw new \Exception('Allowed page limits are [8, 16]');
        }

        $oProducts = $this->select(['id','label','type','download_url','weight','homepage'])
                    ->offset( (($page-1)*$limit) )
                    ->limit($limit);

        $oProducts = $oProducts->where('homepage', 'yes');
                    
        return $oProducts->get();
    }
}
