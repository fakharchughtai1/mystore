<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Cart;

class Order extends Model
{
    use HasFactory;

    protected $fillable = ['customer_details_id', 'shipping_details_id', 'payment_details_id'];

    /**
     * Place order based on products in cart
     */     
    public function placeOrder()
    {
        $cart = new Cart();
        $cartDetails = $cart->getCartDetails();

        if( !isset($cartDetails['items']) || empty($cartDetails['items']) ) {
            throw new \Exception('Cart is empty');
        }

        $orderDetails = Order::create([
            'customer_details_id' => '** reference to customer details **',
            'shipping_details_id' => '** reference to shipping details **',
            'payment_details_id'  => '** reference to payment details **',
        ]);
        
        if( $orderDetails->count() ) {
            foreach($cartDetails['items'] as $item) {
                OrderItem::create([
                    'order_id' => $orderDetails->id,
                    'product_id' => $item->product_id,
                    'quantity' => $item->quantity,
                ]);
            }
        }

        $cart->deleteCart();

        return $this->getOrderDetails($orderDetails->id);
    }

    /**
     * Get details of an order
     */      
    public function getOrderDetails($orderId)
    {
        $orderDetails = Order::where(['id' => $orderId])->first();
        $orderDetails['items'] = OrderItem::with('product')->where(['order_id' => $orderId])->get();
        return  $orderDetails;
    }
}
